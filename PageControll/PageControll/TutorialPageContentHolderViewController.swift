//
//  TutorialPageContentHolderViewController.swift
//  PageControll
//
//  Created by Bruno Rolim on 25/09/15.
//  Copyright (c) 2015 Bruno Rolim. All rights reserved.
//

import UIKit

class TutorialPageContentHolderViewController: UIViewController {

    @IBOutlet weak var myImageView: UIImageView!
    
    var imageFile: String!
    var pageIndex:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myImageView.image = UIImage(named: imageFile)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
