//
//  ViewController.swift
//  PageControll
//
//  Created by Bruno Rolim on 25/09/15.
//  Copyright (c) 2015 Bruno Rolim. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIPageViewControllerDataSource {

    var pageImages:NSArray!
    var pageViewController: UIPageViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageImages = NSArray(objects: "screen1", "screen2", "screen3")
        
        self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MyPageViewController") as! UIPageViewController
        
        self.pageViewController.dataSource = self
        var initialContentViewController = self.pageTutorialAtIndex(0) as TutorialPageContentHolderViewController
        
        var viewControllers = NSArray(object: initialContentViewController)
        self.pageViewController.setViewControllers(viewControllers as [AnyObject], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
        self.pageViewController.view.frame = CGRect(x: 0, y: 100, width: self.view.frame.width, height: self.view.frame.height - 100)
        
        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMoveToParentViewController(self)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageTutorialAtIndex(index: Int)->TutorialPageContentHolderViewController{
        
        var pageContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TutorialPageContentHolderViewController") as! TutorialPageContentHolderViewController
        
        pageContentViewController.imageFile = pageImages[index] as! String
        pageContentViewController.pageIndex = index
        
        return pageContentViewController
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?{
        
        var viewController = viewController as! TutorialPageContentHolderViewController
        
        var index = viewController.pageIndex as Int
        
        if(index==0 || index==NSNotFound){
            return nil
        }
        
        index--
        
        return self.pageTutorialAtIndex(index)
        
    }
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?{
        
        var viewController = viewController as! TutorialPageContentHolderViewController
        
        var index = viewController.pageIndex as Int
        
        if(index==NSNotFound){
            return nil
        }
        
        index++
        
        if(index==pageImages.count){
            return nil
        }
        
        return self.pageTutorialAtIndex(index)
        
    }

    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int{
        return pageImages.count
    }
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int{
       return 0
    }


}

